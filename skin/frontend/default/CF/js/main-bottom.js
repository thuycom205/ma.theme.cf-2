
$j(document).ready(function() {
    $j(".owl-slide-home").owlCarousel({
     
        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        autoPlay: true,
        autoPlay : 4000,
        stopOnHover: true,
        transitionStyle : "fade",
        singleItem:true
    });
     
    $j(".owl-bottom-detail").owlCarousel({
     
        navigation : false, 
     
        navigationText: ["",""],
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
       /* itemsCustom : [
                    [0, 1],
                    [480, 2],
                    [640, 3],
                    [700, 3],
                   
                    [1200, 4],
                  
                    [1600, 4]
                  ],*/
    }); 
    $j(".block-new-product .products-grid").owlCarousel({
     
        navigation : false, 
     
        navigationText: ["",""],
        items : 3,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
       /* itemsCustom : [
                    [0, 1],
                    [480, 2],
                    [640, 3],
                    [700, 3],
                   
                    [1200, 4],
                  
                    [1600, 4]
                  ],*/
    });
    $j(".owl-brand").owlCarousel({
     
        navigation : true, 
        navigationText: ["",""],
        items : 6,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
         
    });
    /*$j(function () {
        var austDay = new Date();
       
        austDay = new Date(2015, 11-1, 17);
        $j('#defaultCountdown').countdown({until: austDay});
        $j('#year').text(austDay.getFullYear());
    });
     */

});

$j(document).ready(function(){
    
    $j(window).on('scroll', function(){
        var secondaryhead = $j('#header'),
    secondaryNavTopPosition = secondaryhead.offset().top;
        if($j(window).scrollTop() > 80 ) {
            setTimeout(function() {
                secondaryhead.addClass('animate-children');
                $j('#header').addClass('fixed');
            }, 50);
        } if($j(window).scrollTop() == 0 ) {
            setTimeout(function() {
                secondaryhead.removeClass('animate-children');
                $j('#header').removeClass('fixed');
            }, 50);
        }
    });

    (function() {
        var isBootstrapEvent = false;
        if (window.jQuery) {
            var all = jQuery('*');
            jQuery.each(['hide.bs.dropdown', 
                'hide.bs.collapse', 
                'hide.bs.modal',
                'hide.bs.tab',
                'hide.bs.popover', 

                'hide.bs.tooltip'], function(index, eventName) {
                all.on(eventName, function( event ) {
                    isBootstrapEvent = true;
                });
            });
        }
        var originalHide = Element.hide;
        Element.addMethods({
            hide: function(element) {
                if(isBootstrapEvent) {
                    isBootstrapEvent = false;
                    return element;
                }
                return originalHide(element);
            }
        });
    })();

    
});
